# flow-test-frontend

## Decisiones de diseño
*  Para el desarrollo del ejercicio, se utilizó ReactJS v16.8, dado que es la tecnología utilizada actualmente por la compañía.
*  Para el testeo del ejercicio, se utilizó JestJS.
*  La API a consumir por la aplicación, ha sido desarrollada en el siguiente repositorio: https://gitlab.com/cptkid/flow-test-backend.


## Análisis preliminar
En una primera aproximación al problema, se revisó la documentación de las tecnologías a utilizar, para entender el alcance de las mismas y la usabilidad aplicable al caso de estudio en cuestión.

## Desarrollo
Los pasos seguidos durante el desarrollo fueron: 
*  Diseño de los componentes.
*  Armado del esqueleto de la Aplicación.
*  Ajuste de los componentes para facilitar la implementación.
*  Escritura de los tests unitarios.
 

Los componentes son:
*  APP
*  +Location - Permite visualizar la información Geolocalizada, así como la hora.
*  +Search - Permite seleccionar la ciudad deseada, de entre las opciones disponibles.
*  +City - Es un wrapper para el pronóstico actual y el pronóstico extendido de la ciudad deseada.
*  +Weather - Es el componente que representa el pronóstico para una fecha y ciudad especificadas.

## Potenciales mejoras
- Búsqueda de cualquier ciudad, para lo que se debe desarrollar un servicio adicional en el Backend en paralelo.
- Mejora de la interfaz gráfica, en términos estéticos.
- Permitir la visualización simultánea de varias ciudades en paralelo.
- Agregar un mapa con la ubicación actual, obtenida a partir de la IP del cliente.
- Incluir comentarios dentro del código fuente para documentar.

# Instalación

## Utilizando Docker (Requiere tener Docker instalado)
Copiar el siguiente script (testeado en UBUNTU 18.04.2) que:
- Clona localmente el repositorio,
- Levanta un contenedor en Docker,
- Ejecuta la aplicación,
- Expone el puerto 


```
git clone https://gitlab.com/cptkid/flow-test-frontend.git
cd flow-test-frontend
npm install
npm install -g serve
npm run build
docker build -t flowtestfront .
docker run -p 5000:5000 flowtestfront
```
Acceder a la aplicación en `http://localhost:5000`


## Utilizando la cli de NPM y ReactJS
Copiar el siguiente script (testeado en UBUNTU 18.04.2) que:
- Clona localmente el repositorio,
- Ejecuta la aplicación,
- Expone el puerto 5000

```
git clone https://gitlab.com/cptkid/flow-test-frontend.git
cd flow-test-frontend
npm install
npm install -g serve
npm run build
serve -s build
```
Acceder a la aplicación en `http://localhost:5000`

## Unit Test
Para ejecutar los tests unitarios sobre la API, copiar el siguiente script (testeado en UBUNTU 18.04.2) que:
- Clona localmente el repositorio,
- Ejecuta JestJS para lanzar los tests a través de los scripts de React cli,
- Imprime en la consola el resultado de los mismos.
```
git clone https://gitlab.com/cptkid/flow-test-frontend.git
cd flow-test-frontend
npm install
npm run test
```



###### Consultas, dudas o problemas, comunicarse a faupablo@gmail.com o contacto@pablo.fau.com.ar