
module.exports = {
  API_URL: 'http://localhost:3000',
  API_VERSION: '/v1',
  TEMP_FACTOR: 273.15,
  ICON_URL_PREFIX: 'https://openweathermap.org/img/wn/',
  ICON_URL_SUFIX: '.png'
};