import React from 'react';
import './App.css';
import Location from './components/Location/';
import City from './components/City/';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      value : ''
    };
  }

  getCity(city){
    this.setState({value: city})
  }
  render() {
    return (
      <div className="App">
        <Location selectedCity={this.getCity.bind(this)} />
        <City city={this.state.value}/>

      </div>
    );
  }
}

export default App;
