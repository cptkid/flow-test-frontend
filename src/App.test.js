import React from 'react';
import {shallow} from 'enzyme';
import ReactDOM from 'react-dom';
import App from './App'
import Location from './components/Location/';
import City from './components/City/';
import Search from './components/Search/';
import Weather from './components/Weather/';


describe('<Location />',()=>{
  it('renders the Location Component',()=>{
    const location = shallow(<Location />);
    expect(location.find('div.test-check').length).toEqual(1);
  })
})


describe('<Search />',()=>{
  it('renders the Search Component',()=>{
    const search = shallow(<Search />);
    expect(search.find('div.test-check').length).toEqual(1);
  })
})



describe('<Weather />',()=>{
  it('renders the Weather Component',()=>{
    const weather = shallow(<Weather />);
    expect(weather.find('div.test-check').length).toEqual(1);
  })
})


describe('<City />',()=>{
  it('renders the City Component',()=>{
    const city = shallow(<City />);
    expect(city.find('div.test-check').length).toEqual(1);
  })
})