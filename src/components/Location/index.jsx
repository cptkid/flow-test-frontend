import React from 'react';
import env from '../../config';
import Search from '../Search/';

class Location extends React.Component {
    intervalID;
    constructor(props) {
      super(props)
        this.state = { 
                        currentLocation: [],
                        currentTime : new Date().toLocaleTimeString()
                      }
    }
    
    componentDidMount() {
      this.intervalID = setInterval(this.getCurrentTime.bind(this), 1000);
      fetch(env.API_URL + env.API_VERSION + '/location')
      .then(res => res.json())
      .then((data) => {
        this.setState({ currentLocation: data })

      })
      .catch(console.log)
    }
  
    getCurrentTime(){
      this.setState({ currentTime: new Date().toLocaleTimeString() })
    }
    propagateCity(city){
        this.props.selectedCity(city);
    }
    render() {
      if (!(this.state.currentLocation.city === undefined)) {
    
        return (
          <div className="location-widget">
          <span>Su ubicación actual es {this.state.currentLocation.city}, {this.state.currentLocation.country}. La hora actual es {this.state.currentTime}.</span>
          <br/><br/>
          <Search emitCity={this.propagateCity.bind(this)} city={this.state.currentLocation.city} country={this.state.currentLocation.country} />
          </div>
        )
      } else {
        return <div>            
                  <div className="test-check" style={{display:"none"}}></div>
                  <p  className="location-widget">Obteniendo datos geográficos...</p>
                </div>
      }
  
    }
  }

  export default Location;
