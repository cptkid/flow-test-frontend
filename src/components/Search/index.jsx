import React from 'react';

class Search extends React.Component {
    constructor(props) {
      super(props)
        this.state = { 
                        value: ''
                      }
    }
    
    change(event){
      this.setState({value: event.target.value});
      this.props.emitCity(event.target.value);

    }
    
    render() {
      
      
      return (
          <div>
            <div className="test-check" style={{display:"none"}}></div>
          <span>Seleccionar Ciudad</span> <select name="city" onChange={this.change.bind(this)} value={this.state.value}>
                                            <option value ={(this.props.city)===undefined?(this.props.city):(this.props.city).replace(/ +/,'+')}>{this.props.city}, {this.props.country}</option>
                                            <option value ="rio+de+janeiro">Rio de Janeiro, Brasil</option>
                                            <option value ="new+york">New York, USA</option>
                                            <option value ="berlin">Berlin, Alemania</option>
                                            <option value ="sidney">Sidney, Australia</option>
                                            <option value ="beijing">Beijing, China</option>
                                          </select> 

          </div>
        )

  
    }
  }

  export default Search;
