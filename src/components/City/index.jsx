import React from 'react';
import env from '../../config';
import Weather from '../Weather/';

class City extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        showForecast:false,
        current: [],
        forecast:[],
        previousCity:''
      };
    }
    componentDidMount(){
        this.setState({previousCity:this.props.city})
        this.fetchData();
    }

    componentDidUpdate() {
        if (this.state.previousCity !== this.props.city){
            this.setState({previousCity:this.props.city,showForecast:false,current:[],forecast:[]})
            this.fetchData();
            //this.showForecastFn();

        }
    }
   
    fetchData() {
        fetch(env.API_URL + env.API_VERSION + '/current/' + (this.props.city || ''))
        .then(res => res.json())
        .then((data) => {
          this.setState({ current: data })
  
        })
        .catch(console.log)
      }
    
    showForecastFn() {
        this.setState({showForecast: !this.state.showForecast});
        fetch(env.API_URL + env.API_VERSION + '/forecast/' + (this.props.city || ''))
        .then(res => res.json())
        .then((data) => {
          this.setState({ forecast: data })
  
        })
        .catch(console.log)
    }
    render() {
        if (!(this.state.current.length === 0)) {

            return (
            <div style={{display:'flex',verticalAlign:'middle', position:'relative'}}>
                            <div className="test-check" style={{display:"none"}}></div>

                <Weather 
                    mainFrame="true" 
                    actualTemp={Math.floor(this.state.current.main.temp-env.TEMP_FACTOR)}
                    maxTemp={Math.floor(this.state.current.main.temp_max-env.TEMP_FACTOR)}
                    minTemp={Math.floor(this.state.current.main.temp_min-env.TEMP_FACTOR)} 
                    pressure={  Math.floor(this.state.current.main.pressure)}
                    humidity={ Math.floor(this.state.current.main.humidity)}
                    icon={this.state.current.weather[0].icon}
                    date={this.state.current.dt*1000}
                    city={this.state.current.name}
                    />
                {(!this.state.showForecast)? 
                    <div>
                        <div className="forecast-button" onClick={this.showForecastFn.bind(this)}>{">"}</div>
                        <div>Ver Forecast</div>
                    </div>
                    :
                    <div>
                        <div className="forecast-button" onClick={this.showForecastFn.bind(this)}>{"<"}
                            </div>
                        <div>Ocultar Forecast</div>
                    </div>
                }
                
                {(this.state.showForecast)?
                    <div>
                    {!(this.state.forecast.length === 0)?
                    <div className="forecast">
                        <Weather 
                            mainFrame="false" 
                            actualTemp={Math.floor(this.state.forecast.list[7].main.temp-env.TEMP_FACTOR)}
                            maxTemp={Math.floor(this.state.forecast.list[7].main.temp_max-env.TEMP_FACTOR)}
                            minTemp={Math.floor(this.state.forecast.list[7].main.temp_min-env.TEMP_FACTOR)} 
                            pressure={  Math.floor(this.state.forecast.list[7].main.pressure)}
                            humidity={ Math.floor(this.state.forecast.list[7].main.humidity)}
                            icon={this.state.forecast.list[7].weather[0].icon}
                            date={this.state.forecast.list[7].dt*1000}
                            />
                        <Weather 
                            mainFrame="false" 
                            actualTemp={Math.floor(this.state.forecast.list[15].main.temp-env.TEMP_FACTOR)}
                            maxTemp={Math.floor(this.state.forecast.list[15].main.temp_max-env.TEMP_FACTOR)}
                            minTemp={Math.floor(this.state.forecast.list[15].main.temp_min-env.TEMP_FACTOR)} 
                            pressure={  Math.floor(this.state.forecast.list[15].main.pressure)}
                            humidity={ Math.floor(this.state.forecast.list[15].main.humidity)}
                            icon={this.state.forecast.list[15].weather[0].icon}
                            date={this.state.forecast.list[15].dt*1000}
                            />
                        <Weather 
                            mainFrame="false" 
                            actualTemp={Math.floor(this.state.forecast.list[16].main.temp-env.TEMP_FACTOR)}
                            maxTemp={Math.floor(this.state.forecast.list[16].main.temp_max-env.TEMP_FACTOR)}
                            minTemp={Math.floor(this.state.forecast.list[16].main.temp_min-env.TEMP_FACTOR)} 
                            pressure={  Math.floor(this.state.forecast.list[16].main.pressure)}
                            humidity={ Math.floor(this.state.forecast.list[16].main.humidity)}
                            icon={this.state.forecast.list[16].weather[0].icon}
                            date={this.state.forecast.list[16].dt*1000}
                            />
                        <Weather 
                            mainFrame="false" 
                            actualTemp={Math.floor(this.state.forecast.list[31].main.temp-env.TEMP_FACTOR)}
                            maxTemp={Math.floor(this.state.forecast.list[31].main.temp_max-env.TEMP_FACTOR)}
                            minTemp={Math.floor(this.state.forecast.list[31].main.temp_min-env.TEMP_FACTOR)} 
                            pressure={  Math.floor(this.state.forecast.list[31].main.pressure)}
                            humidity={ Math.floor(this.state.forecast.list[31].main.humidity)}
                            icon={this.state.forecast.list[31].weather[0].icon}
                            date={this.state.forecast.list[31].dt*1000}
                            />
                        <Weather 
                            mainFrame="false" 
                            actualTemp={Math.floor(this.state.forecast.list[39].main.temp-env.TEMP_FACTOR)}
                            maxTemp={Math.floor(this.state.forecast.list[39].main.temp_max-env.TEMP_FACTOR)}
                            minTemp={Math.floor(this.state.forecast.list[39].main.temp_min-env.TEMP_FACTOR)} 
                            pressure={  Math.floor(this.state.forecast.list[39].main.pressure)}
                            humidity={ Math.floor(this.state.forecast.list[39].main.humidity)}
                            icon={this.state.forecast.list[39].weather[0].icon}
                            date={this.state.forecast.list[39].dt*1000}
                            />
                    </div>
                    :<p  className="location-widget">Obteniendo pronóstico extendido...</p>
                    }
                    </div>
                   :null 
                }
            </div>
            );
        }else{
            return(
                <div >
                                <div className="test-check" style={{display:"none"}}></div>

                    <p  className="location-widget">Obteniendo pronóstico para la ubicación {this.props.city ===''? 'actual':'elegida'}...</p>
                </div>
            );
        }
    }
  }

  export default City