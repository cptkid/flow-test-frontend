import React from 'react';
import env from '../../config';

class Weather extends React.Component {
    
    render() {
        if (!(this.props.pressure === 0)) {

            return (
                <div className="weather-widget">
                                <div className="test-check" style={{display:"none"}}></div>

                    <div className="flexleft">

                    { this.props.mainFrame ==="true" ? 
                    <div>
                        <h4>{this.props.city}</h4>
                        <h2>{this.props.actualTemp}°C</h2>
                    </div>
                    : null }
                        </div>
                    <div className="flexright">
                        <h6>{(new Date(this.props.date)).getDate()+'/'+((new Date(this.props.date)).getMonth()+1)+'/'+(new Date(this.props.date)).getFullYear()}</h6>
                        <img src={env.ICON_URL_PREFIX + this.props.icon + env.ICON_URL_SUFIX} alt={"icon"}/>
                        { this.props.mainFrame ==="false" ?  <div><br/>{this.props.actualTemp}°C <br/></div> : null}
                        <h6><span style={{color:'blue', textAlign:'left'}}>Mín {this.props.minTemp}°C</span></h6>
                        <h6><span style={{color:'red', textAlign:'left'}}>Máx {this.props.maxTemp}°C</span></h6>
                        <h6><span style={{textAlign:'right'}}>Humedad {this.props.humidity}%</span></h6>
                        <h6><span style={{textAlign:'right'}}>Presión {this.props.pressure}hPa</span></h6>
                    </div>


                </div>
            );
        }else{
            return(
                <div className="weather-widget">
                                <div className="test-check" style={{display:"none"}}></div>

                </div>
            );
        }
    }
  }

  export default Weather